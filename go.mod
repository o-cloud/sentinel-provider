module gitlab.com/o-cloud/sentinelprovider

go 1.16

require (
	github.com/gin-gonic/gin v1.7.2
	github.com/mitchellh/mapstructure v1.4.3
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.8.1
	gitlab.com/o-cloud/catalog v0.0.0-20211026092148-685ec4df5feb
	gitlab.com/o-cloud/provider-library v0.0.0-20220304150949-49d5800b2149
)
