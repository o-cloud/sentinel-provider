package provider

import (
	"encoding/xml"
	"time"
)

type OpenSearchResult struct {
	XMLName     xml.Name          `xml:"feed"`
	TotalResult int               `xml:"totalResults"`
	Entry       []OpenSearchEntry `xml:"entry"`
}

type OpenSearchEntry struct {
	Date       time.Time `xml:"date"`
	Resolution float64   `xml:"resolution"`
}
