package provider

import (
	"context"
	"crypto/tls"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	sptypes "gitlab.com/o-cloud/sentinelprovider/types"
)

type SentinelProviderImpl struct {
}

func NewInstance() *SentinelProviderImpl {
	return &SentinelProviderImpl{}
}

func (lp *SentinelProviderImpl) GetMatchCount(ctx context.Context, query sptypes.SearchQueryData) int {
	client := &http.Client{Transport: &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}}
	req, _ := getSearchRequest(query)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Print(err.Error())
		return 0
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error reading response body", err.Error())
		return 0
	}
	var results OpenSearchResult
	xml.Unmarshal(bodyBytes, &results)
	return results.TotalResult
}

func (lp *SentinelProviderImpl) RerserveImages(ctx context.Context, query sptypes.SearchQueryData) sptypes.ReservationData {
	var osResult OpenSearchResult
	reservation := sptypes.ReservationData{Macaroon: ""}
	client := &http.Client{Transport: &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}}
	req, _ := getSearchRequest(query)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("error querying", err.Error())
		return reservation
	}
	defer resp.Body.Close()
	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Print("Error reading body", err.Error())
	}
	xml.Unmarshal(bodyBytes, &osResult)
	log.Printf("received %d images", len(osResult.Entry))
	reservation.Images = make([]sptypes.ImageData, len(osResult.Entry))
	for imageIndex, v := range osResult.Entry {
		bbox := make([]float64, 4)
		bbox[0] = query.BoundingBox.Lng1
		bbox[1] = query.BoundingBox.Lat1
		bbox[2] = query.BoundingBox.Lng2
		bbox[3] = query.BoundingBox.Lat2
		imageQueryData := sptypes.GetImageQueryData{Time: v.Date, BoundingBox: bbox}
		url, _ := getImageRequest(imageQueryData)
		reservation.Images[imageIndex] = sptypes.ImageData{URL: url.URL.String(), Query: imageQueryData}
		reservation.Url = append(reservation.Url, url.URL.String())
	}

	return reservation
}
func (lp *SentinelProviderImpl) GetImage(ctx context.Context, query sptypes.GetImageQueryData) string {
	req, _ := getImageRequest(query)
	return req.URL.String()
}

func getSearchRequest(query sptypes.SearchQueryData) (*http.Request, error) {
	req, err := http.NewRequest("GET", "https://mundiwebservices.com/acdc/catalog/proxy/search/Sentinel2/opensearch", nil)
	if err != nil {
		fmt.Print("error creating request", err.Error())
		return req, err
	}

	bbox := query.BoundingBox
	bboxString := fmt.Sprintf("%f,%f,%f,%f", bbox.Lng1, bbox.Lat1, bbox.Lng2, bbox.Lat2)

	q := req.URL.Query()
	q.Add("maxRecords", "50")
	q.Add("bbox", bboxString)
	q.Add("timeStart", query.DateRange.Start.Format(time.RFC3339))
	q.Add("timeEnd", query.DateRange.End.Format(time.RFC3339))
	q.Add("processingLevel", "L2A")
	q.Add("format", "atom")
	req.URL.RawQuery = q.Encode()
	log.Printf("sending request to mundi")
	return req, nil
}

func getImageRequest(query sptypes.GetImageQueryData) (*http.Request, error) {

	bbox := query.BoundingBox
	bboxString := fmt.Sprintf("%f,%f,%f,%f", bbox[0], bbox[1], bbox[2], bbox[3])
	req, err := http.NewRequest("GET", "http://shservices.mundiwebservices.com/ogc/wms/ea23bfb3-2a67-476f-90e3-fe54873ff897?SERVICE=WMS", nil)
	if err != nil {
		return req, nil
	}
	q := req.URL.Query()
	q.Add("REQUEST", "GetMap")
	q.Add("TRANSPARENT", "true")
	q.Add("LAYERS", "TRUE_COLOR")
	q.Add("VERSION", "1.1.1")
	q.Add("FORMAT", "image%2Fpng")
	q.Add("STYLES", "")
	q.Add("showLogo", "false")
	q.Add("time", query.Time.Format(time.RFC3339))
	q.Add("SRS", "EPSG%3A4326")
	q.Add("RESX", "0.00005")
	q.Add("RESY", "0.00005")
	q.Add("BBOX", bboxString)
	req.URL.RawQuery = q.Encode()
	return req, nil
}
