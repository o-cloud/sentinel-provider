package sptypes

import (
	"context"
	"time"
)

type BoundingBox struct {
	Lat1 float64 `json:"lat1"`
	Lat2 float64 `json:"lat2"`
	Lng1 float64 `json:"lng1"`
	Lng2 float64 `json:"lng2"`
}

type DateRange struct {
	Start time.Time `json:"start"`
	End   time.Time `json:"end"`
}

type SearchQueryData struct {
	DateRange   DateRange   `json:"dateRange"`
	BoundingBox BoundingBox `json:"boundingBox"`
}

type GetImageQueryData struct {
	Time        time.Time
	BoundingBox []float64
}

type ReservationData struct {
	Macaroon string //TODO Replace with true macaroon
	Images   []ImageData
	Url      []string `json:"url"`
}

type ImageData struct {
	Query GetImageQueryData
	URL   string
}

type ISentinelProvider interface {
	DataProviderAPI
}

type DataProviderAPI interface {
	GetMatchCount(ctx context.Context, query SearchQueryData) int
	RerserveImages(ctx context.Context, query SearchQueryData) ReservationData
	GetImage(ctx context.Context, query GetImageQueryData) string
}
