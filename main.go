package main

import (
	libprovider "gitlab.com/o-cloud/provider-library/provider"
	"gitlab.com/o-cloud/sentinelprovider/config"
	"gitlab.com/o-cloud/sentinelprovider/implement"
)

func main() {
	config.Load()

	libprovider.InitProvider(config.Config.Provider)

	implement.RegisterAccessPermission()
	implement.RegisterResourcesRepository()
	implement.RegisterAccessObjectRepository()

	libprovider.RunProvider()

}
