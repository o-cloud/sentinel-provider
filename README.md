Master status:
[![pipeline status](https://gitlab.com/o-cloud/sentinel-provider/badges/develop/pipeline.svg)](https://gitlab.com/o-cloud/sentinel-provider/-/commits/master)
[![coverage report](https://gitlab.com/o-cloud/sentinel-provider/badges/develop/coverage.svg)](https://gitlab.com/o-cloud/sentinel-provider/-/commits/master)

Develop status:
[![pipeline status](https://gitlab.com/o-cloud/sentinel-provider/badges/develop/pipeline.svg)](https://gitlab.com/o-cloud/sentinel-provider/-/commits/develop)
[![coverage report](https://gitlab.com/o-cloud/sentinel-provider/badges/develop/coverage.svg)](https://gitlab.com/o-cloud/sentinel-provider/-/commits/develop)


# Sentinel-Provider
Ce provider met a disposition en tant que dataprovider les images de sentinel

## Functional overview

Exposition de ces endpoint :
- Recherche du nombre d'image disponible 
- Reservation des images
- Récupération de l'url où accèder a l'image

Annonce dans le catalogue :
- S'annonce au catalogue en tant que provider publique
- Fournis une fiche descriptive pour affichage
- Fournis les informations nécéssaire a intégrations dans un workflow



## Technical overview
- Accès à mongodb
- Accès au service catalogue
