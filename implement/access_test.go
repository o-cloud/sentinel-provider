package implement

import (
	"testing"
	"time"

	sptypes "gitlab.com/o-cloud/sentinelprovider/types"
)

func formatQueryDataTest(t *testing.T) {
	queryParam := &sptypes.SearchQueryData{
		BoundingBox: sptypes.BoundingBox{
			Lat1: 10.2,
			Lat2: 10.1,
			Lng1: 10.4,
			Lng2: 10.3,
		},
		DateRange: sptypes.DateRange{
			Start: time.Now(),
			End:   time.Now().Add(1 * time.Hour),
		},
	}

	formatQueryData(queryParam)

	if queryParam.BoundingBox.Lat1 > queryParam.BoundingBox.Lat2 {
		t.Error("Lat1 should be less than Lat2")
	}

	if queryParam.BoundingBox.Lng2 > queryParam.BoundingBox.Lng1 {
		t.Error("Lng1 should be less than Lng2")
	}

}
