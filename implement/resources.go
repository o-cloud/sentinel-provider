package implement

import (
	"gitlab.com/o-cloud/provider-library/models/resources"
)

// DO NOT TOUCH
type resourcesRepository struct {
	resources.DefaultResourcesRepository
}

// DO NOT TOUCH
func RegisterResourcesRepository() {
	resourcesRepository := &resourcesRepository{}
	resourcesRepository.ResourcesRepository = resourcesRepository
	resources.CreateResourcesRepository(resourcesRepository)
}

// TODO : Implement ListResources function that return list of available resources
//        in provider configuration of scopes
// You must not touch to function declaration
func (r *resourcesRepository) ListResources() *[]resources.Resource {
	avResources := []resources.Resource{{Name: "root"}}
	return &avResources
}
