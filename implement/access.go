package implement

import (
	"context"
	"encoding/json"
	"log"
	"math"
	"math/rand"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/o-cloud/provider-library/models/access_object"
	"gitlab.com/o-cloud/provider-library/models/access_tokens"
	"gitlab.com/o-cloud/provider-library/models/scopes"
	"gitlab.com/o-cloud/sentinelprovider/provider"
	sptypes "gitlab.com/o-cloud/sentinelprovider/types"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

type accessObjectController struct {
	Provider sptypes.DataProviderAPI
}

func RegisterAccessObjectRepository() {
	accessObjectController := &accessObjectController{
		Provider: provider.NewInstance(),
	}
	access_object.SetAccessObjectController(accessObjectController)
}

// TODO : Create access object struct with all informations needed to connect on provider
type access struct {
	Url []string `json:"url"`
}

// TODO : Implement CreateAccess function that generate and return access object defined before
// You must not touch to function declaration
func (a *accessObjectController) GenerateAccess(scope *scopes.Scope, accessToken access_tokens.AccessToken) access_object.ConnectionInformation {

	result := &access{
		Url: []string{},
	}
	log.Printf("%#v", accessToken.Params)
	var params sptypes.SearchQueryData
	deoder, _ := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Metadata:         nil,
		WeaklyTypedInput: true,
		DecodeHook:       mapstructure.StringToTimeHookFunc(time.RFC3339),
		Result:           &params,
	})

	err := deoder.Decode(accessToken.Params)
	if err != nil {
		panic(errors.Wrap(err, "invalid params"))
	}

	formatQueryData(&params)

	images := a.Provider.RerserveImages(context.TODO(), params)

	result.Url = removeDuplicateStr(images.Url)

	return result
}

// TODO : Implement PurgeAccess function that purge access that is represented by ConnectionInformation
// You must not touch to function declaration
func (a *accessObjectController) PurgeAccess(connInfo access_object.ConnectionInformation) {
	var access access
	if err := json.Unmarshal(connInfo.([]byte), &access); err != nil {
		panic(err)
	}
}

func formatQueryData(query *sptypes.SearchQueryData) {
	// mundi wants point 1 to be south-west and point 2 to be north-east
	lng1 := math.Min(query.BoundingBox.Lng1, query.BoundingBox.Lng2)
	lng2 := math.Max(query.BoundingBox.Lng1, query.BoundingBox.Lng2)
	lat1 := math.Min(query.BoundingBox.Lat1, query.BoundingBox.Lat2)
	lat2 := math.Max(query.BoundingBox.Lat1, query.BoundingBox.Lat2)

	query.BoundingBox.Lng1 = lng1
	query.BoundingBox.Lng2 = lng2
	query.BoundingBox.Lat1 = lat1
	query.BoundingBox.Lat2 = lat2
}

func removeDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}
