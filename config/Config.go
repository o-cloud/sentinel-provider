package config

import (
	"log"

	"github.com/spf13/viper"
	configProvider "gitlab.com/o-cloud/provider-library/config"
)

var (
	Config GeneralConfig
)

const (
	dockerConfigPath = "/etc/irtsb/"
	localConfigPath  = "./"
)

func Load() {

	//Environment variables
	viper.SetDefault("ProviderName", "SentinelProviderDefault")
	viper.BindEnv("ProviderName", "PROVIDER_NAME")

	viper.SetConfigType("yaml")
	viper.AddConfigPath(dockerConfigPath)
	viper.AddConfigPath(localConfigPath)
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal("Unable to read config file", err)
	}

	// Decode config into ServiceConfig
	if err := viper.Unmarshal(&Config); err != nil {
		log.Fatal("unable to decode configuration", err)
	}

	viper.BindEnv("MONGO_CON_STRING")
	viper.BindEnv("JWT_SECRET_KEY")
	Config.Provider.MongoDatabase.ConString = viper.GetString("MONGO_CON_STRING")
	Config.Provider.JwtSecretKey = viper.GetString("JWT_SECRET_KEY")
}

type GeneralConfig struct {
	Provider configProvider.ProviderConfig
}
